import React from 'react';
import TodoProvider from './src/screens/Todo-App/context/TodoProvider';
import AppNavigator from './src/Navigation/AppNavigator/AppNavigator';

const App = () => {
  return (
    <TodoProvider>
        <AppNavigator />
    </TodoProvider>
  );
};
export default App;

// import React from 'react';
// import AppNavigator from './src/Navigation/AppNavigator/AppNavigator';

// import Home from './src/screens/Home';
// import './src/utils/i18n';

// const App = () => <AppNavigator />;

// export default App;

// import React from 'react';
// import {
//   FlatList,
//   Image,
//   ScrollView,
//   StyleSheet,
//   Text,
//   View,
//   TouchableOpacity,
// } from 'react-native';
// import SIZES from './src/constants/sizes';
// import Icon from 'react-native-vector-icons/Ionicons';

// const logo = {
//   uri: 'https://reactnative.dev/img/tiny_logo.png',
//   width: 64,
//   height: 64,
// };

// const AppBar = () => {
//   const dialogBox = () => {
//     alert('Here we go...');
//   };
//   return (
//     <ScrollView>
//       <View style={styles.AppBar}>
//         <Text style={styles.Reading}>
//           {' '}
//           <Icon name="menu" size={32} />
//         </Text>
//         <Text style={styles.Title}>Title</Text>
//         <Text style={styles.Actions}>
//           <Icon name="search" size={24} />
//           <Icon name="ellipsis-vertical" size={24} />
//         </Text>
//       </View>
//       <View style={styles.container}>
//         <FlatList
//           data={[
//             {key: 'Devin'},
//             {key: 'Dan'},
//             {key: 'Dominic'},
//             {key: 'Jackson'},
//             {key: 'James'},
//             {key: 'Joel'},
//             {key: 'John'},
//           ]}
//           renderItem={({item}) => (
//             <Text>
//               <Image source={logo} style={styles.CircleAvatar} />{' '}
//               <Text style={styles.item}>{item.key}</Text>
//             </Text>
//           )}
//         />
//       </View>
//       <TouchableOpacity style={styles.touchableOpacity} onPress={dialogBox}>
//         <Image source={logo} style={styles.floatingButton} />
//       </TouchableOpacity>
//     </ScrollView>
//   );
// };
// const styles = StyleSheet.create({
//   AppBar: {
//     height: 60,
//     backgroundColor: 'black',
//     flexDirection: 'row',
//     paddingHorizontal: SIZES.SPACING,
//     alignItems: 'center',
//     fontSize: 30,
//   },

//   Reading: {
//     flex: 1,
//     color: 'white',
//   },

//   Title: {
//     flex: 4,
//     textAlign: 'center',
//     color: 'white',
//     fontSize: 24,
//   },

//   Actions: {
//     flex: 1,
//     color: 'white',
//     textAlign: 'right',
//     //padding:
//   },

//   container: {
//     flex: 1,
//     paddingTop: 22,
//   },

//   item: {
//     padding: 10,
//     fontSize: 18,
//     height: 44,
//   },

//   touchableOpacity: {
//     position: 'absolute',
//     width: 50,
//     height: 50,
//     alignItems: 'center',
//     justifyContent: 'center',
//     right: 30,
//     bottom: 30,
//   },

//   floatingButton: {
//     resizeMode: 'contain',
//     width: 50,
//     height: 50,
//   },

//   CircleAvatar: {
//     // height: 100,
//     // width: 100,
//     // borderRadius: 10,
//   },
// });
// export default AppBar;
