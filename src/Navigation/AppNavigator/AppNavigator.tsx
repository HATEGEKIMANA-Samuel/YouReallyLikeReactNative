import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import AppBar from '../../screens/Todo-App/Home';
import CreateHome from '../../screens/Todo-App/CreateHome';
import Task from '../../screens/Todo-App/Task';

const Stack = createNativeStackNavigator();

const AppNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="AppBar"
          component={AppBar}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="second"
          component={CreateHome}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="task"
          component={Task}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
