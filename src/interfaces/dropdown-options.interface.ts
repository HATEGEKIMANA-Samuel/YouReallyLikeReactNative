export interface DropdownOptions {
  key: number;
  label: string;
  value: string;
}
