import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  ScrollView,
  StatusBar,
  TextInput,
  Button,
  Alert,
} from 'react-native';
import {useForm, Controller} from 'react-hook-form';
import Text from '../../components/Text';
import theme from '../../constants/theme';
import styles from './SecondScreen.styles';
import Input from '../../components/Input';
import {t} from 'i18next';

const SecondScreen: React.FC = () => {
  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm({
    defaultValues: {
      firstName: '',
      lastName: '',
    },
  });
  const onSubmit = (data: any) => console.log(data);
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor={theme.COLORS.WHITE} />
      <ScrollView contentInsetAdjustmentBehavior="automatic" style={{top: 200}}>
        <View style={styles.content}>
          <View style={styles.heading}>
            <Text size={24} color={theme.COLORS.ERROR}>
              Create Todo List
            </Text>
            <View>
              <Controller
                control={control}
                rules={{
                  required: true,
                }}
                render={({field: {onChange, onBlur, value}}) => (
                  <Input
                    label={t('shipper')}
                    placeholder={t('companyName')}
                    required
                  />
                )}
                name="firstName"
              />
              {errors.firstName && <Text>This is required.</Text>}

              <Button title="Create" onPress={handleSubmit(onSubmit)} />
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default SecondScreen;
