import { StyleSheet } from 'react-native';

import theme from '../../constants/theme';

const styles = StyleSheet.create({
  flex: {
    ...theme.STYLES.flex,
  },
  row: {
    ...theme.STYLES.row,
  },
  between: {
    justifyContent: 'space-between',
  },
  container: {
    ...theme.STYLES.flex,
    backgroundColor: theme.COLORS.BACKGROUND,
  },
  content: {
    ...theme.STYLES.flex,
    backgroundColor: theme.COLORS.BACKGROUND,
  },
  heading: {
    padding: theme.SIZES.SPACING,
    backgroundColor: theme.COLORS.GRAY,
  },
  form: {
    paddingHorizontal: theme.SIZES.SPACING,
  },
  formHeader: {
    ...theme.STYLES.row,
    ...theme.STYLES.middle,
    paddingVertical: theme.SIZES.SPACING,
  },
  noBorderInput: {
    borderTopWidth: 0,
  },
  optionalInput: {
    marginTop: theme.SIZES.SPACING,
    marginBottom: theme.SIZES.INPUT_SPACING,
  },
  formRow: {
    ...theme.STYLES.row,
    ...theme.STYLES.center,
    justifyContent: 'space-between',
    marginTop: 16,
    marginBottom: theme.SIZES.INPUT_SPACING,
  },
  dropdown: {
    width: (theme.SIZES.windowWidth - theme.SIZES.SPACING * 3) / 2,
  },
  services: {
    marginTop: 16,
  },
  servicesLabel: {
    fontSize: theme.SIZES.BASE,
    lineHeight: 18,
    margin: theme.SIZES.INPUT_SPACING,
  },
  buttons: {
    ...theme.STYLES.row,
    ...theme.STYLES.center,
    justifyContent: 'space-between',
    marginVertical: 32,
  },
  button: {
    width: (theme.SIZES.windowWidth - theme.SIZES.SPACING * 3) / 2,
  },
  backBtnText: {
    color: '#ADCEED',
  },
});

export default styles;
