import React, {useState} from 'react';
import {SafeAreaView, View, ScrollView, StatusBar} from 'react-native';
import {useTranslation} from 'react-i18next';

import Header from '../../components/Header';
import Text from '../../components/Text';
import Input from '../../components/Input';
import Dropdown from '../../components/Dropdown';
import CheckBoxInput from '../../components/CheckBoxInput';
import DatePicker from '../../components/DatePicker';
import Button from '../../components/Button';
import theme from '../../constants/theme';
import styles from './Home.styles';
import Stepper from '../../components/Stepper';
import {useForm, Controller} from 'react-hook-form';

const STEPS = 6;
const LANGS = [
  {key: 1, value: 'en', label: 'English'},
  {key: 2, value: 'kn', label: 'Kinyarwanda'},
];

const Home: React.FC<any> = ({navigation}) => {
  const [language, setLanguage] = useState('en');
  const [currentStep, setCurrentStep] = useState(1);
  const [constructionSite, setConstructionSite] = useState(false);
  const [courierService, setCourierService] = useState(false);
  const [drayageService, setDrayageService] = useState(false);
  const [droppedTrailer, setDroppedTrailer] = useState(false);
  const [insideService, setInsideService] = useState(false);
  const {t, i18n} = useTranslation();

  const changeLanguage = (lng: string) => {
    setLanguage(lng);
    i18n.changeLanguage(lng);
  };

  const next = () => {
    if (currentStep < STEPS) {
      setCurrentStep(currentStep + 1);
    }
  };

  const prev = () => {
    //console.log('Hello world!');
    // navigation.navigate('second');
  };
  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm({
    defaultValues: {
      companyName: '',
      lastName: '',
    },
  });
  const onSubmit = (data: any) => console.log(data);
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor={theme.COLORS.WHITE} />
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <Header />
        <View style={styles.content}>
          <View style={styles.heading}>
            <Text size={24}>{t('createShipment')}</Text>
            <Text bold size={14}>
              {t('steps', {current: 1, total: 6})}
            </Text>
          </View>
          <View>
            <Stepper steps={STEPS} current={currentStep} />
          </View>
          <View style={styles.form}>
            <View style={styles.formHeader}>
              <Text size={14} color={theme.COLORS.ERROR}>
                *
              </Text>
              <Text>{t('indicatesRequiredField')}</Text>
            </View>
            <View style={styles.formRow}>
              <Dropdown
                label="Change Language"
                options={LANGS}
                selectedValue={language}
                onValueChange={val => changeLanguage(val)}
                style={styles.dropdown}
              />
            </View>
            <Controller
              control={control}
              rules={{
                required: true,
                minLength: 3,
              }}
              render={({field: {onChange, onBlur, value}}) => (
                <Input
                  label={t('shipper')}
                  placeholder={t('companyName')}
                  onBlur={onBlur}
                  onChangeText={onChange}
                  value={value}
                />
              )}
              name="companyName"
            />
            {errors.companyName && <Text>This is required.</Text>}
            <Input
              label={t('location')}
              placeholder={t('address')}
              required
              style={styles.noBorderInput}
            />
            <Input
              label={t('bol')}
              placeholder={t('optional')}
              style={styles.optionalInput}
            />
            <View style={styles.formRow}>
              <Dropdown
                label={t('serviceMode')}
                options={[{key: 1, label: 'LTL', value: 'LTL'}]}
                style={styles.dropdown}
              />
              <Dropdown
                label={t('transitService')}
                options={[{key: 1, label: t('selectOne'), value: 'Select One'}]}
                style={styles.dropdown}
              />
            </View>
            <View style={styles.services}>
              <Text bold style={styles.servicesLabel}>
                {t('pickupServices')}
              </Text>
              <CheckBoxInput
                label={t('constructionSite')}
                value={constructionSite}
                onValueChange={value => setConstructionSite(value)}
              />
              <CheckBoxInput
                label={t('courierService')}
                value={courierService}
                onValueChange={value => setCourierService(value)}
              />
              <CheckBoxInput
                label={t('drayageService')}
                value={drayageService}
                onValueChange={value => setDrayageService(value)}
              />
              <CheckBoxInput
                label={t('droppedTrailer')}
                value={droppedTrailer}
                onValueChange={value => setDroppedTrailer(value)}
              />
              <CheckBoxInput
                label={t('insideService')}
                value={insideService}
                onValueChange={value => setInsideService(value)}
              />
            </View>
            <View style={styles.formRow}>
              <DatePicker
                label={t('datePickupRequested')}
                style={styles.dropdown}
              />
              <DatePicker
                label={t('datePickupActual')}
                style={styles.dropdown}
              />
            </View>
            <View style={styles.buttons}>
              <Button
                transparent
                style={styles.button}
                textStyle={styles.backBtnText}
                onPress={prev}>
                {t('back')}
              </Button>
              <Button
                style={styles.button}
                onPress={() => navigation.navigate('second')}>
                {t('next')}
                {/* handleSubmit(onSubmit) */}
                {/* () => navigation.navigate('second') */}
              </Button>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Home;
