import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  ScrollView,
  StatusBar,
  TextInput,
  Button,
  Alert,
  TouchableOpacity,
  Text,
  Pressable,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import CheckBoxInput from '../../../components/CheckBoxInput';
import Input from '../../../components/Input/Input';
import styles from './Task.style';

const Task: React.FC<any> = ({navigation}) => {
    const [droppedTrailer, setDroppedTrailer] = useState(false);
    const [sat, setSat] = useState(false);
    const [epi, setEpi] = useState(false); 
  return (
    <SafeAreaView>
      <ScrollView style={styles.Container}>
        <View>
          <View>
            <View>
              <View style={styles.AppBar}>
                <View>
                  <Text style={styles.Text}>Todo App Series</Text>
                  <Text>5 of 5 tasks</Text>
                </View>
                <TouchableOpacity
                  onPress={() => navigation.navigate('AppBar')}
                  style={{margin: 10, borderRadius: 10}}>
                  <Icon name="close" color="black" size={24} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.line} />
        <View style={styles.container}>
          <CheckBoxInput
            label="Using Firebase Firestore"
            value={droppedTrailer}
            onValueChange={value => setDroppedTrailer(value)}
          />
          <CheckBoxInput
            label="Every Saturday"
            value={sat}
            onValueChange={value => setSat(value)}
          />
          <CheckBoxInput
            label="6 Episodes"
            value={epi}
            onValueChange={value => setEpi(value)}
          />
        </View>
        <TextInput
          style={styles.TextField}
         // onChangeText={text => setname(text)}
          placeholder=" List Name?"
        />
      </ScrollView>
    </SafeAreaView>
  );
};

export default Task;
