import {Dimensions, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  Container: {
    //flex: 1,
    //textAlign: "center",
    //justifyContent: 'center'
    top: Dimensions.get('screen').height / 4,
  },

  TextField: {
    height: 40,
    margin: 10,
    borderRadius: 10,
    borderColor: 'gray',
    borderWidth: 1,
  },

  Text: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 24,
    marginBottom: 20,
  },

  colors: {
    flexDirection: 'row',
    width: Dimensions.get('screen').width,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  touchablecolor: {
    width: 30,
    height: 30,
    backgroundColor: 'purple',
  },
});
export default styles;
