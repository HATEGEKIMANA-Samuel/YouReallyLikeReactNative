import {Dimensions, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  AppBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 20,
  },
  Container: {
    flex: 1,
  },

  Text: {
    textAlign: 'left',
    fontWeight: 'bold',
    fontSize: 24,
    color: 'black',
  },

  line: {
    height: 2,
    backgroundColor: 'blue',
    marginLeft: 20,
  },

  container: {
    flex: 1,
    margin: 20,
  },

  textArea: {
    flexDirection: 'row',
    padding: 30,
  },
  TextField: {
    flex: 1,
    borderColor: 'lightgray',
    borderWidth: 1,
    marginRight: 8,
    height: 50,
  },
  createBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 50,
    height: 50,
  },
});
export default styles;
