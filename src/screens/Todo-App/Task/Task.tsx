import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  View,
  ScrollView,
  StatusBar,
  TextInput,
  Button,
  Alert,
  TouchableOpacity,
  Text,
  Pressable,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import CheckBoxInput from '../../../components/CheckBoxInput';
import Input from '../../../components/Input/Input';
import useTodo from '../context/useTodo';
import styles from './Task.style';

const Task: React.FC<any> = ({navigation, route}) => {
  const [droppedTrailer, setDroppedTrailer] = useState(false);
  const {listId, listName, listColor} = route.params;
  const [tasks, setTasks] = useState<[]>([]);
  const [description, setDescription] = useState(false);
  const {getTask, addTask, updateTaskStatus} = useTodo();

  useEffect(() => {
    const listTask = getTask(listId);

    setTasks(listTask?.tasks ?? []);
  }, [listId, getTask]);

  const createTask = () => {
    addTask(listId, description);
  };

  const updateTask = (taskId, value) => {
    updateTaskStatus(listId, taskId, value);
  };

  return (
    <SafeAreaView style={styles.Container}>
      <View>
        <View>
          <View>
            <View style={styles.AppBar}>
              <View>
                <Text style={styles.Text}>{listName}</Text>
                <Text>
                  {tasks.filter(t => t.status).length} of {tasks.length} tasks
                </Text>
              </View>
              <TouchableOpacity
                onPress={() => navigation.navigate('AppBar')}
                style={{margin: 10, borderRadius: 10}}>
                <Icon name="close" color="black" size={24} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
      <View style={[styles.line, {backgroundColor: listColor}]} />
      <View style={styles.container}>
        <FlatList
          data={tasks}
          keyExtractor={item => `${item.id}`}
          renderItem={({item}) => (
            <CheckBoxInput
              key={item.id}
              label={item.description}
              value={item.status}
              onValueChange={value => updateTask(item.id, value)}
            />
          )}
        />
      </View>
      <View style={styles.textArea}>
        <TextInput
          style={styles.TextField}
          onChangeText={text => setDescription(text)}
          placeholder=""
        />
        <TouchableOpacity
          onPress={createTask}
          style={[styles.createBtn, {backgroundColor: listColor}]}>
          <Icon name="add" color="white" size={24} />
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default Task;
