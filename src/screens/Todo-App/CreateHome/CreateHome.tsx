import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  ScrollView,
  StatusBar,
  TextInput,
  Button,
  Alert,
  TouchableOpacity,
  Text,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import colors from '../../../constants/colors';
import useTodo from '../context/useTodo';
import styles from './CreateHome.styles';

const Colors = [
  {
    color: 'green',
  },
  {
    color: 'skyblue',
  },
  {
    color: 'blue',
  },
  {
    color: 'indigo',
  },
  {
    color: 'purple',
  },
  {
    color: 'red',
  },
  {
    color: 'orange',
  },
];
const CreateHome: React.FC<any> = ({navigation}) => {
  const [color, setcolor] = useState('orange');
  const [name, setname] = useState('');
  const {addTodo} = useTodo();
  const createTodo = () => {
    addTodo(name, color);
  };
  return (
    <SafeAreaView>
      <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={{margin: 10, borderRadius: 10}}>
        <Icon
          name="close"
          color="black"
          size={24}
          style={{textAlign: 'right'}}
        />
      </TouchableOpacity>
      <ScrollView style={styles.Container}>
        <View>
          <View>
            <View>
              <Text style={styles.Text}>Create Todo List</Text>
              <TextInput
                style={styles.TextField}
                onChangeText={text => setname(text)}
                placeholder=" List Name?"
              />
              <View style={styles.colors}>
                {Colors.map(color => (
                  <TouchableOpacity
                    onPress={() => setcolor(color.color)}
                    style={[
                      styles.touchablecolor,
                      {backgroundColor: color.color},
                    ]}
                  />
                ))}
              </View>
              <View style={{margin: 10, borderRadius: 10}}>
                <Button title="Create" color={color} onPress={createTodo}>
                  create
                </Button>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default CreateHome;
