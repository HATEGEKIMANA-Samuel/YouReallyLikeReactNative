import React, {useState} from 'react';
import TodoContext from './TodoContext';

const TodoProvider: React.FC<any> = ({children}) => {
  const [todos, setTodos] = useState([]);
  const addTodo = (name, color) => {
    setTodos([...todos, {title: name, color, id: Math.random(), tasks: []}]);
  };

  const getTask = (listId: number) => {
    return todos.find(task => task.id === listId);
  };

  const addTask = (listId, description) => {
    const newTodo = todos.map(todo =>
      todo.id !== listId
        ? todo
        : {
            ...todo,
            tasks: [
              ...todo.tasks,
              {description, id: Math.random(), status: false},
            ],
          },
    );
    setTodos(newTodo);
  };

  const updateTaskStatus = (listId, taskId, status) => {
    const newTodo = todos.map(todo =>
      todo.id !== listId
        ? todo
        : {
            ...todo,
            tasks: todo.tasks.map(task =>
              task.id !== taskId ? task : {...task, status},
            ),
          },
    );
    setTodos(newTodo);
  };

  const removeTodo = (id: number) => {
    const newTodo = todos.filter(todo => todo.id !== id);

    setTodos(newTodo);
  };

  const values = {
    todos,
    addTodo,
    getTask,
    addTask,
    updateTaskStatus,
    removeTodo,
  };

  return <TodoContext.Provider value={values}>{children}</TodoContext.Provider>;
};
export default TodoProvider;
