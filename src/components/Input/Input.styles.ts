import { StyleSheet } from 'react-native';

import theme from '../../constants/theme';

const styles = StyleSheet.create({
  item: {
    ...theme.STYLES.row,
    height: 52,
    borderWidth: 1,
    borderColor: theme.COLORS.INPUT_BORDER,
  },
  labelView: {
    justifyContent: 'center',
    width: theme.SIZES.windowWidth * 0.25,
    paddingHorizontal: theme.SIZES.INPUT_SPACING,
    borderRightWidth: 1,
    borderRightColor: theme.COLORS.INPUT_BORDER,
  },
  inputView: {
    ...theme.STYLES.flex,
    ...theme.STYLES.row,
    width: '100%',
  },
  input: {
    ...theme.STYLES.flex,
    paddingHorizontal: theme.SIZES.INPUT_SPACING,
    color: theme.COLORS.DEFAULT,
    fontSize: 16,
    textDecorationColor: 'transparent',
    textShadowColor: 'transparent',
  },
  inputIcon: {
    marginHorizontal: theme.SIZES.BASE,
  },
  label: {
    fontSize: theme.SIZES.BASE,
    lineHeight: 18,
  },
  asterisk: {
    marginHorizontal: theme.SIZES.INPUT_SPACING,
    marginTop: 2,
  },
});

export default styles;
