import React from 'react';
import {
  View,
  TextInput,
  ViewStyle,
  TextInputProps,
  ViewProps,
} from 'react-native';

import Text from '../Text';
import theme from '../../constants/theme';
import styles from './Input.styles';

export type InputProps = TextInputProps &
  ViewProps & {
    label: string;
    placeholder: string;
    required?: boolean;
    style?: ViewStyle;
  };

const Input: React.FC<InputProps> = ({
  label,
  placeholder,
  required,
  style,
  ...rest
}) => (
  <View style={[styles.item, style]}>
    <View style={styles.labelView}>
      <Text bold style={styles.label}>
        {label}
      </Text>
    </View>
    <View style={styles.inputView}>
      <TextInput
        style={styles.input}
        placeholder={placeholder}
        placeholderTextColor={theme.COLORS.PLACEHOLDER}
        selectionColor={theme.COLORS.PRIMARY}
        underlineColorAndroid="transparent"
        {...rest}
      />
      {required ? (
        <Text size={14} color={theme.COLORS.ERROR} style={styles.asterisk}>
          *
        </Text>
      ) : null}
    </View>
  </View>
);

export default Input;
