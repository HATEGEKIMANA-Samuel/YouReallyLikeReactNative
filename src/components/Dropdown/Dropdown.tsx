import React from 'react';
import { View, ViewStyle, ViewProps } from 'react-native';
import { Picker, PickerProps } from '@react-native-picker/picker';

import { DropdownOptions } from '../../interfaces/dropdown-options.interface';
import Text from '../Text';
import theme from '../../constants/theme';
import styles from './Dropdown.styles';

export type DropdownProps = PickerProps &
  ViewProps & {
    label: string;
    options: DropdownOptions[];
    style?: ViewStyle;
  };

const Dropdown: React.FC<DropdownProps> = ({
  label,
  options,
  style,
  ...rest
}) => (
  <View style={[styles.item, style]}>
    <View style={styles.labelView}>
      <Text bold style={styles.label}>
        {label}
      </Text>
    </View>
    <View style={styles.pickerView}>
      <Picker
        mode="dropdown"
        style={styles.picker}
        dropdownIconColor={theme.COLORS.DEFAULT}
        {...rest}>
        {options.map(item => (
          <Picker.Item key={item.key} label={item.label} value={item.value} />
        ))}
      </Picker>
    </View>
  </View>
);

export default Dropdown;
