import React, { useState } from 'react';
import { View, Pressable, Platform, ViewStyle, ViewProps } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import DateTimePicker from '@react-native-community/datetimepicker';

import Text from '../Text';
import theme from '../../constants/theme';
import styles from './DatePicker.styles';

export type DatePickerProps = ViewProps & {
  label: string;
  style?: ViewStyle;
};

const DatePicker: React.FC<DatePickerProps> = ({ label, style, ...rest }) => {
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);

  const onChange = selectedDate => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
  };

  const showDatepicker = () => {
    setShow(true);
  };

  return (
    <View style={[styles.item, style]}>
      <View style={styles.labelView}>
        <Text bold style={styles.label}>
          {label}
        </Text>
      </View>
      <Pressable style={styles.pickerView} onPress={showDatepicker}>
        <Text bold numberOfLines={1} style={styles.picker}>
          Select Date
        </Text>
        <Icon name="caret-down" size={14} color={theme.COLORS.DEFAULT} />
      </Pressable>
      {show && (
        <DateTimePicker
          value={date}
          mode="date"
          is24Hour={true}
          display="default"
          onChange={onChange}
          {...rest}
        />
      )}
    </View>
  );
};

export default DatePicker;
