import React from 'react';
import {View, Pressable, ViewProps, ViewStyle, Image, Text} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import theme from '../../constants/theme';
import styles from './Header.styles';

export type HeaderProps = ViewProps & {
  style?: ViewStyle;
};

const Header: React.FC<HeaderProps> = ({style}) => (
  <View style={[styles.header, style]}>
    <View style={styles.leftView}>
      <Pressable style={styles.button}>
        <Icon name="menu" size={32} color={theme.COLORS.DEFAULT} />
      </Pressable>
    </View>
    <View style={styles.centerView}>
      <Text>$</Text>
    </View>
    <View style={styles.rightView}>
      <Pressable style={styles.profile}>
        <Image
          source={require('../../../assets/images/user.png')}
          style={styles.thumbnail}
        />
        <Icon name="caret-down" size={14} color={theme.COLORS.DEFAULT} />
      </Pressable>
    </View>
  </View>
);

export default Header;
