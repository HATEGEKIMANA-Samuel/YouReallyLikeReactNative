import 'react-i18next';
import en from '../translations/languages/en.json';
import kn from '../translations/languages/kn.json';

declare module 'react-i18next' {
  interface CustomTypeOptions {
    defaultNS: 'en';
    resources: {
      en: typeof en;
      kn: typeof kn;
    };
  }
}
